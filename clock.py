import unittest
from datetime import datetime, timedelta


class Clock:
    def __init__(self, hours, minutes):
        self._base_time = datetime.now().replace(hour=0, minute=0)
        self._hours = hours
        self._minutes = minutes
        self.hours()
        self.minutes()

    def hours(self):
        self._base_time += timedelta(hours=self._hours)
        return self

    def sum(self, minutes):
        self._base_time += timedelta(minutes=minutes)
        return self

    def minutes(self):
        self.sum(self._minutes)
        return self

    def run(self):
        return self._base_time.strftime('%H:%M')


class TestClock(unittest.TestCase):
    def test_one(self):
        self.assertEqual(Clock(8, 0).run(), '08:00')

    def test_two(self):
        self.assertEqual(Clock(72, 8640).run(), '00:00')

    def test_three(self):
        self.assertEqual(Clock(-1, 15).run(), '23:15')

    def test_four(self):
        self.assertEqual(Clock(-25, -160).run(), '20:20')

    def test_sum_method(self):
        self.assertEqual(Clock(10, 3).sum(-70).run(), '08:53')


suite = unittest.TestLoader().loadTestsFromTestCase(TestClock)
unittest.TextTestRunner(verbosity=2).run(suite)
