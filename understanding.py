# -*- coding: utf-8 -*-
import unittest


def z(c):
    # si el ultimo carácter es "?" retorna 3 de lo contrario retorna 2
    if c[-1] == '?':
        return 3
    else:
        return 2


def y(b):
    # genera una lista con True o False
    # validando que sea un carácter alfanumérico y mayúscula (excepto el último carácter)
    r = [l.isupper() for l in b[:-1] if l.isalpha()]

    # ! podría fallar el reduce si en r es una lista vacía
    # pero se encuentra validado que r una lista vacía
    if r and reduce(lambda a, x: a and x, r):
        """
            retorna 4 si la lista generada en "r" solo contiene True
            el reduce se podría haber optimizado de esta forma
                all(r)
                o
                next((a for a in r if not a), True)
            y de esta forma no es necesario recorrer todos los carácteres
        """
        return 4
    else:
        return z(b)


def x(a):
    # limpia los espacios en blanco
    a = ''.join(a.strip().split(' '))
    # retorna 1 si es un string vacío o nulo de lo contrario continua y()
    return y(a) if a else 1


# my solution
def c(c):
    return 3 if c[-1] == '?' else 2


def b(b):
    return 4 if b[:-1].isupper() else c(b)


def a(a):
    return b(a.replace(' ', '')) if a else 1


class TestUnderstanding(unittest.TestCase):
    def test_one(self):
        self.assertEqual(a(''), x(''))

    def test_two(self):
        self.assertEqual(a('1 2 3 4'), x('1 2 3 4'))

    def test_three(self):
        self.assertEqual(a('1 2 3 4 A?'), x('1 2 3 4 A?'))

    def test_four(self):
        self.assertEqual(a('hola?'), x('hola?'))

    def test_sum_method(self):
        self.assertEqual(a('HOLA'), x('HOLA'))


suite = unittest.TestLoader().loadTestsFromTestCase(TestUnderstanding)
unittest.TextTestRunner(verbosity=2).run(suite)
