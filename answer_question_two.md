Si alguna vez has utilizado la API de Facebook, sabrás lo común que "deprecaban" su API. Según usted:

* Explique la razón por la cuál esta no es una buena práctica.
Al deprecar una api y no versionarla, las aplicaciones que dependen también deprecaran.

* ¿Por qué se dice que las interfaces deben ser "tontas"?
Para que los usuarios que van a utilizarla puedan entenderlas facilmente.

* Como aplicaria usted los conceptos de Encapsulamiento, Flexibilidad, Seguridad y Versionamiento en el desarrollo de una Interfaz?
Utilizando framework de desarrollo acorde a la interface que deseo realizar.